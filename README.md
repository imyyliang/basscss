# 精简CSS开发：原子类库的轻便之选

在现代Web开发中，CSS样式的管理是开发者工作中不可或缺的一环。原子类库通过提供简单、可重用的类名，极大地简化了样式的定义和维护过程。在这篇文章中，我们将介绍我们的原子类库，并将其与其他流行的库，包括Tailwind CSS和UnoCSS进行对比。

仓库地址：https://gitee.com/imyyliang/basscss

css 文件下载：https://gitee.com/imyyliang/basscss/raw/master/dist/basscss.min.css

## 选择轻便的原子类库

### Tailwind CSS

Tailwind CSS是一款流行的CSS框架，也采用了原子类的概念。相较于我们的原子类库，Tailwind更加庞大且功能丰富。它提供了大量的工具类，使得开发者能够在不编写自定义CSS的情况下创建出几乎任何可能的样式。这使得Tailwind非常适用于大型项目和团队协作。

然而，对于一些小型项目或个人开发者来说，引入Tailwind可能会显得过于臃肿。在这种情况下，我们的原子类库作为一个更轻量级的解决方案，更注重简洁性和易用性，使得入门变得更加轻松，同时保留了足够的灵活性。

### UnoCSS

UnoCSS采用一种有趣的方法，通过分析HTML页面中的样式并删除未使用的样式，实现样式的优化。与我们的原子类库不同，UnoCSS更注重减小样式表的体积，提高页面加载性能。它通过精简样式来实现优化，而不是提供更多样式选项。

我们的原子类库在这方面提供了一种更加直接的选择。我们的目标是在不引入庞大的原子类库的前提下，提供一个小巧而方便使用的工具库，使得样式的管理更加简便。

## 我们的CSS原子类库

我们的原子类库提供了一系列清晰易读的样式选项，包括文本装饰、对齐、空白字符、行高、行截断、列表样式、显示、溢出、宽度、边距、填充、Flexbox、位置、Z-index、边框、边框半径以及不显示等。以下是一些主要的类别：

### 文本装饰类

- `.text-decoration-none`: 无文本装饰
- `.bold`: 粗体文本
- `.normal`: 普通文本
- `.italic`: 斜体文本

### 文本对齐类

- `.left-align`: 左对齐文本
- `.center`: 居中文本
- `.right-align`: 右对齐文本

### 空白字符类

- `.nowrap`: 不换行
- `.break-word`: 换行时在单词间断开

### 行高类

- `.line-height-1`: 行高1档
- `.line-height-2`: 行高2档
- `.line-height-3`: 行高3档
- `.line-height-4`: 行高4档

### 行截断类

- `.line-clamp-1`: 最多显示1行
- `.line-clamp-2`: 最多显示2行
- `.line-clamp-3`: 最多显示3行

### 列表样式类

- `.list-reset`: 重置列表样式

### 显示类

- `.inline`: 行内显示
- `.block`: 块级显示
- `.inline-block`: 行内块级显示

### 溢出类

- `.overflow-hidden`: 溢出隐藏
- `.overflow-scroll`: 溢出滚动
- `.overflow-auto`: 自动处理溢出

### 宽度类

- `.fit-width`: 宽度自适应内容

### 外边距类

- `.m0`: 无外边距
- `.mx-middle`: 水平方向居中外边距

### 内边距类

- `.p0`: 无内边距

### Flexbox类

- `.flex`: 弹性布局
- `.flex-column`: 纵向排列
- `.flex-wrap`: 换行
- `.items-start`, `.items-end`, `.items-center`, `.items-baseline`, `.items-stretch`: 项目在交叉轴上的对齐方式
- `.justify-start`, `.justify-end`, `.justify-center`, `.justify-between`, `.justify-around`, `.justify-evenly`: 项目在主轴上的对齐方式
- `.content-start`, `.content-end`, `.content-center`, `.content-between`, `.content-around`, `.content-stretch`: 多轴线的对齐方式
- `.flex-auto`: 自动分配剩余空间

### 位置类

- `.relative`, `.absolute`, `.fixed`, `.sticky`: 定位方式
- `.top-0`, `.right-0`, `.bottom-0`, `.left-0`: 定位偏移

### Z-index类

- `.z1`, `.z2`, `.z3`: 设置层级

### 边框类

- `.border`: 默认边框
- `.border-none`: 无边框
- `.border-top`, `.border-right`, `.border-bottom`, `.border-left`: 单边边框

### 边框半径类

- `.rounded`: 圆角边框
- `.circle`: 圆形边框

### 不显示类

- `.display-none`: 不显示元素

## 如何使用

要使用我们的原子类库，只需在HTML元素上应用相应的类名即可。例如，如果您想要一个居中对齐、粗体的标题，只需添加类名 `.center.bold` 到标题元素上即可。

```html
<h1 class="center bold">欢迎使用我们的网站</h1>
```

通过这样简单的操作，您就能轻松实现各种样式需求，而无需编写大量自定义的CSS代码。

## 结语

在选择CSS原子类库时，要根据项目的规模和需求来权衡轻重。我们的原子类库致力于提供一种小巧而方便使用的工具，使得在不引入庞大框架的前提下，开发者能够更轻松地管理样式。我们理解庞大的框架并非适用于所有场景，因此提供了一种更为轻便的选择。在实际开发中，选择适合自己项目的工具才是最明智的决策。

---

**原项目介绍, 我在这个库的基础上按自己需要改的**

# Basscss

Low-level CSS toolkit – the original [Functional CSS][functional-css] library <https://basscss.com>

[![Build Status](https://travis-ci.org/basscss/basscss.svg)](https://travis-ci.org/basscss/basscss)
[![npm version](https://badge.fury.io/js/basscss.svg)](https://badge.fury.io/js/basscss)

## Lightning-Fast Modular CSS with No Side Effects

Basscss is a lightweight collection of immutable utilities designed for speed, clarity, performance, and scalability.

## Stable

Basscss v8 is the final version of Basscss, which means no major, breaking changes will be introduced.
Minor features and patches may be added, but
due to the nature of this CSS approach, there are virtually no bugs in Basscss.

---

## Features

### Code with Confidence

Using clear, humanized naming conventions, Basscss is quick to internalize
and easy to reason about while speeding up development time with more scalable,
more readable code.

### No Side Effects

Things behave exactly as expected with immutable utilities
and styles that follow the open/closed principle
to help prevent common pitfalls with CSS.

### Composable

Reusable, interoperable styles
work like building blocks to lay the foundation for any stylesheet
and can be mixed and matched in any number of combinations.

## Designed for Design

Basscss strikes a balance between consistency and flexibility
to allow for rapid prototyping and quick iterative changes
when designing in the browser.

## Responsive by Default

Basscss provides lightweight, performant styles
and flexible utilities to design for any device
and to help reduce boilerplate in stylesheets.

## Unassuming

Modular and customizable typography and layout styles don’t dictate
what things should look like and play well with other stylesheets and frameworks.

[Read More](https://basscss.com)

---

## Other Packages

### Base styles

The core Basscss package does not include any base element styles.
For an out-of-the-box solution, see:

https://github.com/basscss/basic

### Addons

In addition to the core modules, optional modules,
including responsive margin, padding, layout, and typography styles, can be found here:

https://github.com/basscss/addons

### Ace.css

For a bundle with the core Basscss and all optional modules, see:

https://github.com/basscss/ace

---

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## Related

- [Tachyons][tachyons]
- [Buzzfeed Solid][solid]
- [OOCSS][oocss]

## Thanks

This library was largely inspired and influenced by the following people

- [Nicole Sullivan](https://twitter.com/stubbornella)
- [Nicolas Gallagher](https://twitter.com/necolas)
- [Adam Morse](https://twitter.com/mrmrs_)
- [Jessica Harllee](https://twitter.com/harllee)
- [Zack Sears](https://twitter.com/zacksears)
- [Diana Mounter](https://twitter.com/broccolini)
- [John Otander](https://twitter.com/4lpine)

---

[MIT license](LICENSE.md)

[functional-css]: https://jon.gold/2015/07/functional-css/
[tachyons]: http://tachyons.io
[solid]: https://solid.buzzfeed.com
[oocss]: https://github.com/stubbornella/oocss/wiki
