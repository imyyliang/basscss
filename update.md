再次修改，删除数字相关的东西，比如边距啊字体大小啊，应该每个项目自己定义自己的原子类，颜色也是，我的想法是这个原子类集合的作用，就仅仅是将经常用又不会变的集合在一起

---

再次修改，删除了不少很少用的样式，浮动之类

---

下面是之前的修改想法

---



### 1. 边框圆角调整

我们已经修改了默认边框圆角的弧度，以提供更灵活的设计选项。现在，750 设计稿中的边框圆角被设置为 6px，并且编译成了 vw 单位，以确保在不同设备上呈现一致。

### 2. Flex 弹性盒子布局

我们对 flex 弹性盒子布局的原子类进行了更新，以便更好地支持不同设备大小。现在，我们引入了以下断点：

* 小屏幕：@custom-media --breakpoint-sm (min-width: 576px);
* 中等屏幕：@custom-media --breakpoint-md (min-width: 992px);
* 大屏幕：@custom-media --breakpoint-lg (min-width: 1200px);

### 3. Grid 栅格化布局

对于 grid 栅格化布局的原子类，我们也进行了类似的更新，引入了相同的设备大小断点，以确保在各种屏幕上实现灵活的布局。

### 4. Hide 隐藏样式

针对 hide 隐藏样式，我们引入了以下设备大小的断点：

* 超小屏幕：@custom-media --breakpoint-xs (max-width: 575px);
* 小到中等屏幕：@custom-media --breakpoint-sm-md (min-width: 576px) and (max-width: 991px);
* 中等到大屏幕：@custom-media --breakpoint-md-lg (min-width: 992px) and (max-width: 1199px);
* 大屏幕：@custom-media --breakpoint-lg (min-width: 1200px);

### 5. 内外边距基础单位

我们对内外边距的基础单位进行了更新，提供了更灵活的设置。以下是新的基础单位，并且它们已经编译成了 vw 单位：

* --space-1: 10px;
* --space-2: 24px;
* --space-3: 32px;
* --space-4: 48px;

### 6. Z-index 默认节点

现在，z-index 的原子类具有以下默认节点：

* --z1: 1;
* --z2: 9;
* --z3: 99;
* --z4: 999;

### 7. 字体大小原子类

我们对字体大小进行了更新，并编译成了 vw 单位。以下是新的字体大小原子类：

* --h00: 64px;
* --h0: 48px;
* --h1: 32px;
* --h2: 24px;
* --h3: 20px;
* --h4: 16px;
* --h5: 14px;
* --h6: 12px;

### 8. 文字设定更新

* 支持 regular 和 normal 原子类设置普通粗细。
* 添加了第五级 line-height-5 的原子类，为2倍行高。

### 9. 文字截断原子类

在原来的 truncate 原子类下面，我们添加了 line-clamp-1 到 line-clamp-5 的原子类，用于多行文字截断。
